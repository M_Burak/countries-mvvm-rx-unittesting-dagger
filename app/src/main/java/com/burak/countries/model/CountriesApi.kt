package com.burak.countries.model

import io.reactivex.Single
import retrofit2.http.GET

interface CountriesApi {

    @GET("Devtides/countries/master/countriesV2.json")
    fun getCountries(): Single<List<Country>>
}