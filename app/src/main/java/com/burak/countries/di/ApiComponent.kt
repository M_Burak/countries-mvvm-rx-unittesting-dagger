package com.burak.countries.di

import com.burak.countries.model.CountriesService
import com.burak.countries.viewmodel.ListViewModel
import dagger.Component

@Component(modules = [ApiModule::class])
interface ApiComponent {

    fun inject(service: CountriesService)

    fun injectListViewModel(listViewModel: ListViewModel )
}