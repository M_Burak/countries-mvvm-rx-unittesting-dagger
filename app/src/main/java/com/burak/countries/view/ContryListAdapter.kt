package com.burak.countries.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.burak.countries.R
import com.burak.countries.model.Country
import com.burak.countries.util.getProgressDrawable
import com.burak.countries.util.loadImage
import kotlinx.android.synthetic.main.item_country.view.*

class ContryListAdapter(var countries: ArrayList<Country>) :
    RecyclerView.Adapter<ContryListAdapter.CountryViewHolder>() {

    fun updateCountries(newCountries: List<Country>) {
        countries.clear()
        countries.addAll(newCountries)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CountryViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_country, parent, false)
    )

    override fun getItemCount() = countries.size

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        holder.bind(countries[position])
    }

    class CountryViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val countryName = view.tvName
        private val flag = view.ivFlag
        private val capital = view.tvCapital
        private val progressDrawable = getProgressDrawable(view.context)
        fun bind(country: Country) {
            countryName.text = country.countryName
            capital.text = country.capital
            flag.loadImage(country.flag,progressDrawable)
        }
    }
}