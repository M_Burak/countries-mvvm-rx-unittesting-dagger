package com.burak.countries.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.burak.countries.R
import com.burak.countries.viewmodel.ListViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var viewModel: ListViewModel
    private val countriesAdapter = ContryListAdapter(arrayListOf())
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)
        viewModel.refresh()

        rwCountries.apply{
            layoutManager = LinearLayoutManager(context)
            adapter = countriesAdapter
        }


        swRefresh.setOnRefreshListener {
            swRefresh.isRefreshing = false
            viewModel.refresh()
        }
        observeViewModel()
    }

    fun observeViewModel(){
        viewModel.countries.observe(this, Observer { countries ->
            countries?.let{
                rwCountries.visibility = View.VISIBLE
                countriesAdapter.updateCountries(it)
            }
        })

        viewModel.countryLoadError.observe(this, Observer { isError->
            isError?.let { liestError.visibility = if (it) View.VISIBLE else View.GONE  }
        })

        viewModel.loading.observe(this, Observer { progress ->
            progress?.let { progressCircular.visibility = if (it) View.VISIBLE else View.GONE
                if(it){
                    liestError.visibility=View.GONE
                    rwCountries.visibility=View.GONE
                }
            }
        })
    }
}
